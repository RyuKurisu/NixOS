{ config, pkgs, ... }:

{ services.xserver.displayManager.lightdm.enable = true;    # Enable LightDM
  services.xserver.desktopManager.lxqt.enable = true;       # Enable the LXQt Desktop Environment

  # Packages to exclude from the default selection
  environment.lxqt.excludePackages = with pkgs; [
    lxqt.qterminal
  ];

  # Packages to add to the default selection
  environment.systemPackages = with pkgs; [
    #vim 
    qpdfviewer
  ];
}
