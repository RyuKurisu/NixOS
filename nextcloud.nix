{config, pkgs, ...}: { 

networking.firewall.allowedTCPPorts = [ 80 443 ];

security.acme = {
  acceptTerms = true;
  email = "chrisdruif@tutanota.com";
};

# Enable Nginx
services.nginx = {
  enable = true;

# Use recommended settings
  recommendedGzipSettings = true;
  recommendedOptimisation = true;
  recommendedProxySettings = true;
  recommendedTlsSettings = true;
  
# Only allow PFS-enabled ciphers with AES256#ff607d8b
  sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";
# Setup Nextcloud virtual host to listen on ports
  virtualHosts = {
    
    "nextcloud.automated-things.ml" = {
      ## Force HTTP redirect to HTTPS with LetsEncrypt
      forceSSL = true;
      enableACME = true;
    };
  };
};

# Actual Nextcloud Config
services.nextcloud = {
  enable = true;
  hostName = "nextcloud.automated-things.ml";
  # Enable built-in virtual host management
  # Takes care of somewhat complicated setup
  # See here: https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/services/web-apps/nextcloud.nix#L529
  nginx.enable = true;
  
  # Use HTTPS for links
  https = true;
  # Auto-update Nextcloud Apps
  autoUpdateApps.enable = true;
  # Set what time makes sense for you
  autoUpdateApps.startAt = "03:00:00";
  
  config = {
    # Further forces Nextcloud to use HTTPS
    overwriteProtocol = "https";
    
    # Nextcloud PostegreSQL database configuration, recommended over using SQLite 
    dbtype = "pgsql";
    dbuser = "nextcloud";
    dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
    dbname = "nextcloud";
    dbpassFile = "/var/nextcloud-db-pass";
    adminuser = "admin";
    adminpassFile = "/var/nextcloud-admin-pass";
  };
};

# Enable PostgreSQL
services.postgresql = {
  enable = true;
  
  # Ensure the database, user, and permissions always exist
  ensureDatabases = [ "nextcloud" ];
  ensureUsers = [ {
    name = "nextcloud";
    ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES"; 
  } ];
}; 

# Ensure that postgres is running before running the setup
systemd.services."nextcloud-setup" = {
  requires = ["postgresql.service"];
  after = ["postgresql.service"];
  };
}
